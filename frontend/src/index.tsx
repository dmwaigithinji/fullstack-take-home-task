import React from 'react';
import ReactDOM from 'react-dom/client';
import { ApolloProvider } from '@apollo/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { ThemeProvider } from '@mui/material';

import client from './graphql/apollo-client';

import HomePage from './pages/Home';
import NotFoundPage from './pages/NotFound';
import Books from './pages/Books';
import ReadingLists from './pages/ReadingLists';
import ReadingListDetail from './pages/ReadingListDetail';

import { theme } from './styles/theme';
import './styles/index.css';

const router = createBrowserRouter([
  {
    path: '/',
    element: <HomePage />,
    errorElement: <NotFoundPage />,
    children: [
      {
        path: '/',
        element: <Books />
      },
      {
        path: '/reading-lists',
        element: <ReadingLists />
      },
    ]
  },
  {
    path: '/reading-list/:listName',
    element: <ReadingListDetail />
  }
]);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
);
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <RouterProvider router={router} />
      </ApolloProvider>
    </ThemeProvider>
  </React.StrictMode>,
);
