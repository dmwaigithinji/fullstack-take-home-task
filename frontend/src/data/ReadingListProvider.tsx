import { useCallback, useEffect, useMemo } from 'react';
import { useQuery } from '@apollo/client';
import { Box, Button, CircularProgress, Typography } from '@mui/material';
import { GET_READING_LIST } from '../graphql/queries';
import useReadingListStore from './readingLists.store';
import { Book } from '../types/Book.interface';

const PAGE_SIZE = 18;

/**
 * Handles how data is queried, paginated in reading list detail pages.
 *
 * It also handles displaying of error and loading status.
 */
const ReadingListProvider: React.FC<{
  listName: string;
}> = ({ listName }) => {
  const {
    setLoading,
    addBooks,
    currentPage,
    setCurrentPage,
    resetReadingListStore,
  } = useReadingListStore(
    (state) => ({
      setLoading: state.setLoading,
      addBooks: state.addReadingListBooks,
      currentPage: state.currentPage,
      setCurrentPage: state.setCurrentPage,
      resetReadingListStore: state.resetReadingListStore
    })
  );

  const pageNumber = useMemo(() => currentPage || 1, [currentPage])
  const { data, loading, error, fetchMore } = useQuery(
    GET_READING_LIST,
    {
      variables: {
        listName,
        paginationArgs: {
          page: pageNumber,
          pageSize: PAGE_SIZE,
        },
      },
    fetchPolicy: 'network-only'
  });

  useEffect(() => {
    const fetchedBooks: Book[]  = data?.readingList?.books?.books || [];
    addBooks(fetchedBooks);
  }, [data]);

  useEffect(() => {
    setLoading(loading);
  }, [loading, setLoading]);

  useEffect(() => {
    return () => resetReadingListStore();
  }, []);

  const showLoadMore = useMemo(() => {
    const totalPages = data?.readingList?.books?.pageInfo?.totalPages || 0;
    return totalPages > pageNumber;
  }, [data?.readingList?.books?.pageInfo?.totalPages, pageNumber])

  const handleLoadMore = useCallback(() => {
    const nextPage = pageNumber + 1;
    fetchMore({
      variables: {
        listName,
        paginationArgs: {
          page: nextPage,
          pageSize: PAGE_SIZE,
        },
      },
    });
    setCurrentPage(nextPage);
  }, [pageNumber, fetchMore, listName, setCurrentPage]);

  if (!!error) {
    return (
      <Typography
        textAlign="center"
        color="error"
      >
        Oops! <br /> <small>An error occurred while loading data.</small>
      </Typography>
    );
  }

  if (loading) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', mb: 3 }}>
        <CircularProgress />
      </Box>
    );
  }

  if (showLoadMore) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center', mb: 3 }}>
        <Button
          variant='text'
          onClick={handleLoadMore}>
          Load More
        </Button>
      </Box>
    );
  }

  return null;
}

export default ReadingListProvider;
