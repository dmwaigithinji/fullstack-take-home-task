import {create} from 'zustand';
import { Book } from '../types/Book.interface';

interface ReadingListState {
  readingListBooks: Book[];
  addReadingListBooks: (newReadingListBooks: Book[]) => void;
  addBook: (book: Book) => void;
  removeBook: (bookTitle: string, author: string) => void;
  resetReadingListStore: () => void;

  loading: boolean;
  setLoading: (loading: boolean) => void;

  currentPage: number;
  setCurrentPage: (page: number) => void;
}

/**
 * Manages the state for reading list detail pages.
 */
const useReadingListStore = create<ReadingListState>((set) => ({
  readingListBooks: [],
  addReadingListBooks: (newBooks) =>
    set((state) => ({ readingListBooks: [...state.readingListBooks, ...newBooks]})),
  addBook: (book) =>
    set((state) => ({ readingListBooks: [...state.readingListBooks, book]})),

  removeBook: (bookTitle, author) => set((state) => {
    const current = state.readingListBooks || [];
    const updated = current.filter((book) => !(book.title === bookTitle && book.author === author))
    return { readingListBooks: updated}
  }),

  currentPage: 1,
  setCurrentPage: (currentPage) => set({currentPage}),
  resetReadingListStore: () => set({readingListBooks: [], currentPage: 1}),

  loading: true,
  setLoading: (loading) => set({loading}),
}));

export default useReadingListStore;
