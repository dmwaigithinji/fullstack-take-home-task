import { useCallback, useMemo, useState } from 'react';
import { ApolloError, useQuery } from '@apollo/client';
import { GET_BOOKS, GET_READING_LEVELS } from '../graphql/queries';
import { useDebounce } from './useDebounce';
import { Book } from '../types/Book.interface';

const PAGE_SIZE = 18;

interface UsePaginatedBooksProps {
  initialSearchText?: string;
  initialPageSize?: number;
  initialReadingLevel?: string;
}

interface UsePaginatedBooksResult {
  books: Book[];
  loading: boolean;
  error: ApolloError | undefined;
  showLoadMore: boolean;
  handleLoadMore: () => void;
  searchText: string;
  setSearchText: (text: string) => void;
  readingLevel: string;
  setReadingLevel: (level: string) => void;
  readingLevels: string[];
}

/**
 * Manages querying and pagination of books and reading levels available.
 *
 * It also manages searching and filtering of books.
 */
export const usePaginatedBooks = ({
  initialPageSize = PAGE_SIZE,
}: UsePaginatedBooksProps): UsePaginatedBooksResult => {
  const [searchText, setSearchText] = useState('');
  const [readingLevel, setReadingLevel] = useState('');
  const debouncedSearchText = useDebounce(searchText, 500);

  const { data: readingLevelsData } = useQuery(GET_READING_LEVELS);

  const { loading, error, data, fetchMore  } = useQuery(GET_BOOKS, {
    variables: {
      paginationArgs: { page: 1, pageSize: initialPageSize },
      search: { bookTitle: debouncedSearchText },
      filters: { readingLevel },
    },
    fetchPolicy: 'cache-and-network',
  });

  const books = useMemo(() => data?.books?.items || [], [data]);

  const {showLoadMore, nextPageNumber} = useMemo(() => {
    const totalPages = data?.books?.pageInfo?.totalPages || 0;
    const currentPage = data?.books?.pageInfo?.currentPage;
    const showLoadMore = !loading && totalPages > currentPage;
    return { showLoadMore, nextPageNumber: currentPage + 1 };
  }, [data, loading]);

  const handleLoadMore = useCallback(() => {
    fetchMore({
      variables: {
        paginationArgs: { page: nextPageNumber, pageSize: initialPageSize },
        search: { bookTitle: debouncedSearchText },
        filters: { readingLevel },
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return previousResult;
        return {
          ...fetchMoreResult,
          books: {
            ...fetchMoreResult.books,
            items: [
              ...previousResult.books.items,
              ...fetchMoreResult.books.items,
            ],
          },
        };
      },
    });
  }, [fetchMore, nextPageNumber, initialPageSize, debouncedSearchText, readingLevel]);

  return {
    books,
    loading,
    error,
    showLoadMore,
    handleLoadMore,
    searchText,
    setSearchText,
    readingLevel,
    setReadingLevel,
    readingLevels: readingLevelsData?.readingLevels || [],
  };
};
