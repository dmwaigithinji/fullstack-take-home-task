import { gql } from '@apollo/client';

export const ADD_BOOK_TO_READING_LIST = gql`
  mutation AddBookToReadingList($listName: String!, $bookTitle: String!, $author: String!) {
    addBookToReadingList(listName: $listName, bookTitle: $bookTitle, author: $author) {
      listName
      addedBook {
        title
        author
        coverPhotoURL
        readingLevel
        id
      }
    }
  }
`;

export const REMOVE_BOOK_FROM_READING_LIST = gql`
  mutation RemoveBookFromReadingList($listName: String!, $bookTitle: String!, $author: String!) {
    removeBookFromReadingList(listName: $listName, bookTitle: $bookTitle, author: $author) {
      listName
      removed
    }
  }
`;