import { gql } from '@apollo/client';

export const GET_BOOKS = gql`
  query GetBooks($paginationArgs: PaginationArgs, $search: Search, $filters: Filters) {
    books(paginationArgs: $paginationArgs, search: $search, filters: $filters) {
      items: books {
        id
        title
        author
        coverPhotoURL
        readingLevel
      }
      pageInfo {
        pageSize
        currentPage
        totalPages
      }
    }
  }
`;

export const GET_READING_LISTS = gql`
  query ReadingLists {
    readingLists {
      name
      books {
        books {
          coverPhotoURL
        }
      }
    }
  }
`;

export const GET_READING_LIST = gql`
  query ReadingList($listName: String!, $paginationArgs: PaginationArgs) {
    readingList(listName: $listName, paginationArgs: $paginationArgs) {
      books {
        books {
          id
          title
          author
          coverPhotoURL
          readingLevel
        }
        pageInfo {
          pageSize
          currentPage
          totalPages
        }
      }
    }
  }
`;

export const GET_READING_LEVELS = gql`
  query ReadingLists {
    readingLevels
  }
`;

export const GET_BOOK_BY_ID = gql`
  query GetBookById($id: String!) {
    bookById(id: $id) {
      id
      title
      author
      coverPhotoURL
      readingLevel
      pages
    }
  }
`;