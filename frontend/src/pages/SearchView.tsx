import { useCallback, useMemo } from 'react';
import { useMutation } from '@apollo/client';
import { Box, TextField, IconButton, Typography, Button, Dialog, DialogTitle, DialogContent, Container, useTheme, useMediaQuery } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { ADD_BOOK_TO_READING_LIST, REMOVE_BOOK_FROM_READING_LIST } from '../graphql/mutations';
import useReadingListStore from '../data/readingLists.store';
import { Book, DisplayBook } from '../types/Book.interface';
import BookList from '../components/BookList';
import { usePaginatedBooks } from '../hooks/usePaginatedBooks';
import ReadingLevelsChips from '../components/ReadingLevelsChips';

const SearchView: React.FC<{
  listName: string;
  selectedTitles: {[title: string]: boolean}
  visible: boolean;
  onClose: () => void
}> = ({ listName, selectedTitles, visible, onClose }) => {

  const [addBookToReadingList] = useMutation(ADD_BOOK_TO_READING_LIST);
  const [removeBookFromReadingList] = useMutation(REMOVE_BOOK_FROM_READING_LIST);
  const {addBook, removeBook} = useReadingListStore((state) => ({
    addBook: state.addBook,
    removeBook: state.removeBook,
  }));

  const {
    books,
    loading,
    error,
    showLoadMore,
    handleLoadMore,
    searchText,
    setSearchText,
    readingLevels,
    readingLevel,
    setReadingLevel
  } = usePaginatedBooks({});

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const handleAdd = useCallback(async (bookTitle: string, author: string) => {
    const { data } = await addBookToReadingList({
      variables: {
        listName,
        bookTitle,
        author
      }
    });
    const addedBook = data.addBookToReadingList.addedBook;
    delete addedBook.__typename;
    addBook(addedBook as Book);
  }, [addBook, addBookToReadingList, listName]);

  const handleRemove = useCallback(async (bookTitle: string, author: string) => {
    const { data } = await removeBookFromReadingList({
      variables: {
        listName,
        bookTitle,
        author
      }
    });
    const removed = data.removeBookFromReadingList.removed;
    removed && removeBook(bookTitle, author);
  }, [removeBookFromReadingList, listName, removeBook]);

  const displayBooks: DisplayBook[] = useMemo(() => {
      return books.map(book => ({
        ...book,
        actionText: selectedTitles[book.title] ? 'Remove' : 'Add',
        action: selectedTitles[book.title] ? handleRemove : handleAdd,
      }))
    },
    [books, selectedTitles, handleRemove, handleAdd]
  )

  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Dialog fullScreen fullWidth open={visible} onClose={onClose}>
      <DialogTitle>
        <Container sx={{ padding: isSmallScreen ? 0 : 2 }}>
          <Typography color={'textPrimary'} textAlign={'center'} sx={{ fontSize: 24, fontWeight: 'bold' }}>
            Add Books to {listName}
          </Typography>
          <Box sx={{ mt: 3, mb: 2, display: 'flex', alignItems: 'center' }}>
            <TextField
              fullWidth
              size='small'
              value={searchText}
              onChange={handleSearchChange}
              placeholder="Search books by title..."
              variant="outlined"
              InputProps={{
                endAdornment: (
                  searchText
                    ? (<Button variant='text' onClick={() => setSearchText('')}> Clear </Button>)
                    : null
                ),
              }}
            />
            <IconButton onClick={onClose} size="large" sx={{ ml: 2 }}>
              <CloseIcon />
            </IconButton>
          </Box>
          <ReadingLevelsChips
            readingLevels={readingLevels}
            selectedReadingLevel={readingLevel}
            onClick={setReadingLevel}
          />
        </Container>
      </DialogTitle>
      <DialogContent>
        <Container sx={{ padding: 0 }}>
          <BookList
            displayBooks={displayBooks}
            error={error}
            loading={loading}
            showLoadMore={showLoadMore}
            handleLoadMore={handleLoadMore}
          />
        </Container>
      </DialogContent>
    </Dialog>
  );
};

export default SearchView;
