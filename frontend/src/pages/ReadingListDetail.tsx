import { Fragment, useCallback, useMemo, useState } from 'react';
import { useMutation } from '@apollo/client';
import { useParams, useNavigate } from 'react-router-dom';
import { Box, Button, Container, Typography } from '@mui/material';
import ArrowBack from '@mui/icons-material/ArrowBack';
import Title from '../components/Title';
import BookGrid from '../components/BookGrid';
import SearchView from './SearchView';
import { REMOVE_BOOK_FROM_READING_LIST } from '../graphql/mutations';
import useReadingListStore from '../data/readingLists.store';
import NoBooks from '../components/NoBooks';
import ReadingListProvider from '../data/ReadingListProvider';

type Mode = 'view' | 'add' | 'remove';

const ReadingListDetail: React.FC = () => {
  const {listName} = useParams<{listName: string}>();
  const {readingListBooks, removeBook, loading} = useReadingListStore((state) => ({
    readingListBooks: listName ? (state.readingListBooks) : [],
    removeBook: state.removeBook,
    loading: state.loading,
  }));

  const [removeBookFromReadingList] = useMutation(REMOVE_BOOK_FROM_READING_LIST);
  const [mode, setMode] = useState<Mode>('view');

  const navigate  = useNavigate();
  const handleBack = () => {
    navigate('/reading-lists');
  };

  const handleRemove = useCallback(async (bookTitle: string, author: string) => {
    const { data } = await removeBookFromReadingList({
      variables: {
        listName,
        bookTitle,
        author
      }
    });
    const removed = data.removeBookFromReadingList.removed;
    removed && removeBook(bookTitle, author);
  }, [listName]);

  const selectedTitles = useMemo(() => (
    readingListBooks.reduce((titlesObj, book) => {
      titlesObj[book.title] = true;
      return titlesObj;
    }, {} as {[title: string]: boolean})
  ), [readingListBooks])

  const displayBooks = useMemo(() => {
    return readingListBooks.map(book => ({
      ...book,
      actionText: 'Remove',
      action: handleRemove
    }))
  }, [readingListBooks])

  if (!listName) return null;

  return (
    <Fragment>
      {mode === 'add' && (
        <SearchView
          listName={listName}
          selectedTitles={selectedTitles}
          visible={mode === 'add'}
          onClose={setMode.bind(null, 'view')}
        />
      )}
      <Container sx={{ px: 2, pb: 4 }}>
        <Box
          sx={{
            position: 'sticky',
            top: 0,
            backgroundColor: 'white',
            zIndex: 1,
            pt: 3,
            pb: 2,
          }}
        >
          <Button
            variant='text'
            startIcon={<ArrowBack color="primary" />}
            color='primary'
            onClick={handleBack}
            size='small'
          >
            <Typography color="primary">
              Back to Reading Lists
            </Typography>
          </Button>
          <Box sx={{
            mt: 4,
            mb: 3,
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
          <Title title={listName} />
          <Button
            variant='outlined'
            onClick={setMode.bind(null, 'add')}
          >
            <Typography color="primary">
              Add Books
            </Typography>
          </Button>
          </Box>
        </Box>
        <BookGrid books={displayBooks} />
      </Container>

      {
        !displayBooks.length &&
        !loading &&
        <NoBooks
          message="You can start adding books to the list!"
          imageSrc='/assets/addBook.webp'
        />}
      <ReadingListProvider listName={listName} />
    </Fragment>
  );
};

export default ReadingListDetail;
