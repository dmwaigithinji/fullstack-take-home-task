import { useQuery } from '@apollo/client';
import { Box, Container, Grid } from '@mui/material';
import { GET_READING_LISTS } from '../graphql/queries';
import Title from '../components/Title';
import ReadingListCard from '../components/ReadingListCard';
import { Book } from '../types/Book.interface';
import LoadingGrid from '../components/LoadingGrid';

const ReadingLists: React.FC = () => {
  const { loading, error, data } = useQuery(GET_READING_LISTS, {fetchPolicy: 'cache-and-network'});

  if (error) return <p>Error! </p>;

  return (
    <Container sx={{ padding: 2 }}>
      <Box sx={{ mb: 4 }}>
        <Title title='Reading Lists' />
      </Box>

      {loading && <LoadingGrid skeletonCount={4} height={200} />}

      {!loading && (
        <Grid container spacing={3}>
          {data.readingLists.map(
            (list: { name: string; noOfBooks: number, books: any }, index: number) => (
              <Grid
                key={index}
                item
                xs={6} sm={4} md={3}
                >
                <ReadingListCard
                  key={list.name}
                  listName={list.name}
                  coverPhotoURLs={list.books.books.map((b: Book) => b.coverPhotoURL)}
                />
              </Grid>
            ),
            )}
        </Grid>
      )}
    </Container>
  );
};

export default ReadingLists;
