import { Box, Button, Typography } from "@mui/material";

export default function NotFoundPage() {

  return (
    <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh'}}>
      <Box sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        p: 5,
        textAlign: 'center'
      }}>
        <img src="/assets/404.webp" alt="404" width="200" height="auto" />

        <Typography
          variant='h5'
          sx={{ my: 2.5, letterSpacing: '0.18px' }}>
          It seems you've wandered too far
        </Typography>
        <Typography sx={{ mb: 2.5 }}>We couldn&prime;t find the page you are looking for :(</Typography>
        <Button href="/" variant="contained" color="primary" size="large">
          Go Back
        </Button>
      </Box>
    </Box>
  );
}