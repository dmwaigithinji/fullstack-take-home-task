import {Outlet} from "react-router-dom";
import AppBarComponent from "../components/Header";
import { Box } from "@mui/material";

export default function HomePage() {
  return (
    <div>
      <AppBarComponent />
      <Box sx={{ mt: 1 }}>
        <Outlet />
      </Box>
    </div>
  );
}