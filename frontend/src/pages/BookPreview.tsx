import { useMemo, useState } from 'react';
import { Typography, Box, Avatar, CircularProgress, IconButton, Dialog, DialogTitle, DialogContent, Container } from '@mui/material';
import { ArrowBack, ArrowForward, Close } from '@mui/icons-material';
import { useQuery } from '@apollo/client';
import { GET_BOOK_BY_ID } from '../graphql/queries';
import { Book } from '../types/Book.interface';

const styles = {
  backgroundContainer: {
    position: 'relative',
    height: '100%',
    backgroundImage: 'url("/assets/bookPageBackground.png")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'bottom center',
    backgroundSize: 'contain',
    marginBottom: '-60px'
  },
};

const BookPreview: React.FC<{
  bookId: string;
  visible: boolean;
  onClose: () => void
}> = ({
  bookId, visible, onClose
}) => {
  const { loading, error, data } = useQuery(GET_BOOK_BY_ID, {
    variables: { id: bookId },
  });

  const [page, setPage] = useState(0);

  const book = data?.bookById as Book;

  const fontSize = useMemo(() => {
    if (!book) return '';
    const textLength = book.pages[0].join('').length;
    if (textLength <= 50) return '36px';
    if (textLength <= 100) return '28px';
    if (textLength <= 150) return '24px';
    return '22px';
  }, [book]);

  if (loading) return null;
  if (error) {
    console.log('Error loading book preview');
    return null;
  }


  const handlePrevPage = () => setPage(page - 1);
  const handleNextPage = () => setPage(page + 1);

  return (
    <Dialog fullScreen open={visible}>
      <DialogTitle>
        <TopBar book={book} onClose={onClose} />
      </DialogTitle>
      <DialogContent sx={{ margin: 0, padding: 0 }}>
        <Container sx={styles.backgroundContainer}>
          <Box sx={{ py: 4, px: 2, maxWidth: '600px' }}>
            {book.pages[page].map((text, i) => (
              <Typography key={i} sx={{ fontSize, paddingBottom: 3 }}>
                {text}
              </Typography>
            ))}
          </Box>
          {book.pages.length > 1 && (
            <NavigationButtons
              page={page}
              totalPages={book.pages.length}
              onPrevPage={handlePrevPage}
              onNextPage={handleNextPage}
            />
          )}
        </Container>
      </DialogContent>
    </Dialog>
  );
}

export default BookPreview;

const TopBar: React.FC<{ book: Book, onClose: () => void}> = ({
  book,
  onClose
}) => {
  return (
    <Container sx={{ display: 'flex', justifyContent: 'space-between', pt: 2, px: 0 }}>
      <Box>
        <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
          <Avatar alt="Book Icon" src={`/${book.coverPhotoURL}`} sx={{ width: 40, height: 40 }} />
          <Box>
            <Typography variant="h6">
              {book.title}
            </Typography>
            <Typography color='GrayText' fontSize={14}>
              Level {book.readingLevel}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box>
        <IconButton onClick={onClose}>
          <Close />
        </IconButton>
      </Box>
    </Container>
  )
}

interface NavigationButtonProps {
  page: number;
  totalPages: number;
  onPrevPage: () => void;
  onNextPage: () => void;
}
const NavigationButtons: React.FC<NavigationButtonProps> = ({
  page, totalPages, onPrevPage, onNextPage }) => {
  return (
    <Box sx={{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      mt: 2,
      px: 2,
      maxWidth: '500px',
    }}>
      <Box>
        {page > 0 && (
          <IconButton onClick={onPrevPage} color='secondary'>
            <ArrowBack />
          </IconButton>
        )}
      </Box>
      <Typography color={'GrayText'}>
        Page {page + 1}/{totalPages}
      </Typography>
      <Box>
        {page < totalPages - 1 && (
        <IconButton onClick={onNextPage} color='secondary'>
          <ArrowForward />
        </IconButton>
        )}
      </Box>
    </Box>
  );
};
