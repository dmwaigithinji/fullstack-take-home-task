import { Box, Container } from '@mui/material';

import { usePaginatedBooks } from '../hooks/usePaginatedBooks';
import Title from '../components/Title';
import BookList from '../components/BookList';
import ReadingLevelsChips from '../components/ReadingLevelsChips';

const Books: React.FC = () => {
  const {
    books,
    loading,
    error,
    showLoadMore,
    handleLoadMore,
    readingLevels,
    readingLevel,
    setReadingLevel
  } = usePaginatedBooks({});

  return (
    <Container sx={{ px: 2 }}>
      <Box
        sx={{
          position: 'sticky',
          top: 0,
          backgroundColor: 'white',
          zIndex: 1,
          pt: 1,
          pb: 2,
        }}
      >
        <Box sx={{ mb: 2 }}>
          <Title title='All Books' />
        </Box>
        <ReadingLevelsChips
          readingLevels={readingLevels}
          selectedReadingLevel={readingLevel}
          onClick={setReadingLevel}
        />
      </Box>
      <BookList
        displayBooks={books}
        error={error}
        loading={loading}
        showLoadMore={showLoadMore}
        handleLoadMore={handleLoadMore}
      />
    </Container>
  );
};

export default Books;
