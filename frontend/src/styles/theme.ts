import { createTheme  } from '@mui/material'

export const theme = createTheme ({
  palette: {
    common: {
      black: '#335c6e', // Steel blue
    },
    primary: {
      main: '#5ACCCC', // Turquoise
      dark: '#4AA088', // Teal
    },
    secondary: {
      main: '#F76A34' // Orange Red
    },
    text: {
      primary: '#335c6e', // Steel blue
      secondary: '#F76A34' // Yellow dark
    }
  },
  typography: {
    fontFamily: 'Mulish',
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 600,
    fontWeightBold: 700,
    allVariants: {
      color: '#2C3232',
    },
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableElevation: true
      },
      styleOverrides: {
        root: {
          textTransform: 'none',
          borderRadius: '50px',
        },
        containedPrimary: {
          color: 'white',
        },
        containedSecondary: {
          color: 'white',
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: '50px',
        },
      },
    },
    MuiChip: {
      styleOverrides: {
        colorPrimary: {
          color: 'white',
        },
        colorSecondary: {
          color: 'white',
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          padding: 0,
        },
      }
    }
  },
})