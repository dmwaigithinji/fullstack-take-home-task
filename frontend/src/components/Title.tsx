import { Typography } from '@mui/material';

interface PropType {
  title: string;
};

const Title: React.FC<PropType> = ({ title }) => {
  return (
    <Typography
      color="textPrimary"
      variant="h4"
      component="h1"
      sx={{ fontWeight: 'bold' }}>
      {title}
    </Typography>
  );
}

export default Title;
