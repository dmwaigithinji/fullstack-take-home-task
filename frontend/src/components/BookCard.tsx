import { Fragment, memo, useState } from 'react';
import { Badge, Box, Button, Card, CardMedia, Tooltip, Typography } from '@mui/material';
import BookPreview from '../pages/BookPreview';

interface PropType {
  id: string;
  title: string;
  author: string;
  coverPhotoURL: string;
  readingLevel?: string;
  actionText?: string;
  action?: (bookTitle: string, author: string) => void
}

const BookCard: React.FC<PropType> = (
  { id, title, author, coverPhotoURL, actionText, readingLevel, action }
) => {
  const [showPreview, setShowPreview] = useState(false)
  return (
    <Fragment>
      <Card
        sx={{ maxWidth: 345, boxShadow: 'none', borderRadius: 0, position: 'relative' }}>
        <Button onClick={setShowPreview.bind(null, true)} sx={{ m: 0, p: 0, textAlign: 'start' }}>
          <Box>
            <Tooltip title={"Reading Level - " + readingLevel}>
              <Badge
                overlap="circular"
                color="default"
                badgeContent={readingLevel}
                sx={{
                  position: 'absolute',
                  top: 16,
                  right: 16,
                  '& .MuiBadge-badge': {
                    backgroundColor: 'rgba(0, 0, 0, 0.4)',
                    color: 'white',
                    fontSize: 12,
                    fontWeight: 'bold',
                    width: 16,
                    height: 20,
                    borderRadius: '50%',
                    zIndex: 0,
                  },
                }}
              >
              </Badge>
            </Tooltip>
            <CardMedia
              component="img"
              height="150"
              image={`/${coverPhotoURL}`}
              alt={title}
              loading='lazy'
            />
            <Box sx={{ marginTop: 2 }}>
              <Typography sx={{ fontSize: '14px', fontWeight: '700' }} >
                {title}
              </Typography>
              <Typography variant="body2" color="text.disabled">
                by {author}
              </Typography>
            </Box>
          </Box>
          </Button>
        {action && actionText && (
          <Box sx={{ pt: 1 }}>
            <Button
              variant='outlined'
              color={actionText === 'Remove' ? 'warning' : 'primary'}
              onClick={action.bind(null, title, author)}
              size='small'
            >
              {actionText}
            </Button>
          </Box>
        )}
      </Card>
      {showPreview && <BookPreview
        bookId={id}
        visible={showPreview}
        onClose={setShowPreview.bind(null, false)}
      />}
    </Fragment>
  );
}

export default memo(BookCard);
