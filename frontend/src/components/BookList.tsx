import { Fragment, memo } from 'react';
import { ApolloError } from '@apollo/client';
import { Box, Button, CircularProgress, Typography } from '@mui/material';
import BookGrid from './BookGrid';
import { DisplayBook } from '../types/Book.interface';
import NoBooks from './NoBooks';

interface BookListProps {
  displayBooks: DisplayBook[];
  error: ApolloError | undefined;
  loading: boolean;
  showLoadMore: boolean;
  handleLoadMore: () => void
}

const BookList: React.FC<BookListProps> = ({
  displayBooks, error, loading, showLoadMore, handleLoadMore
}) => {
  if (error) return <Typography>Error</Typography>;

  return (
    <Fragment>
      <Box sx={{ mt: 3, mb: 5 }}>
        <BookGrid books={displayBooks} />
      </Box>

      {!displayBooks.length && !loading && (
        <NoBooks />
      )}

      {showLoadMore && (
        <Box sx={{ display: 'flex', justifyContent: 'center', my: 4 }}>
          <Button variant="text" onClick={handleLoadMore} disabled={loading}>
            Load More
          </Button>
        </Box>
      )}

      {loading && (
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', mt: 5 }}>
          <CircularProgress />
        </Box>
      )}
    </Fragment>
  );
};

export default memo(BookList);
