import { Box, Typography } from "@mui/material";

const NoBooks: React.FC<{imageSrc?: string, message?: string}> = ({
  message,
  imageSrc = '/assets/404.webp'
}) => {
  return (
    <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '70vh'}}>
      <Box sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        p: 5,
        textAlign: 'center'
      }}>
        <img src={imageSrc} alt="No Book" width="200" height="auto" />
        <Typography
          variant='h6'
          color='textPrimary'
          sx={{ mt: 3 }}>
          { message || 'No books found!' }
        </Typography>
      </Box>
    </Box>
  );
}

export default NoBooks;