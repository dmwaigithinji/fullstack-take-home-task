import { Grid, Skeleton } from "@mui/material";

interface PropType {
  skeletonCount: number;
  height: number;
}

const LoadingGrid: React.FC<PropType> = ({skeletonCount, height}) => {

  return (
    <Grid container spacing={2}>
      {Array.from(new Array(skeletonCount)).map((_, index) => (
        <Grid item xs={6} sm={4} md={3} lg={2} key={index}>
          <Skeleton variant="rectangular" width="100%" height={height} animation="wave" />
          <Skeleton variant="text" width="60%" />
          <Skeleton variant="text" width="40%" />
        </Grid>
      ))}
    </Grid>
  )
}

export default LoadingGrid;