import { useMemo } from 'react';
import { NavLink } from 'react-router-dom';
import { AppBar, Toolbar, Box, Typography } from '@mui/material';

const AppBarComponent = () => {
  const links = useMemo(() => {
    return [
      {
        title: 'Books',
        path: '/'
      }, {
        title: 'Reading Lists',
        path: '/reading-lists'
      }
    ]
  }, []);

  return (
    <AppBar position="sticky" sx={{ boxShadow: 'none', background: '#FFFFFF' }}>
      <Toolbar sx={{ display: 'flex', justifyContent: 'space-between'}}>
        <img src="/logo.svg" alt="logo" width="auto" height="50" />

        <Box sx={{ display: 'flex', alignItems: 'center', gap: 4, paddingRight: 2, mt: 1 }}>
          {
            links.map(link => (
              <NavLink
                key={link.title}
                to={link.path}
                style={({ isActive }) => ({
                  textDecoration: 'none',
                  color: isActive ? '#5ACCCC' : '#335c6e',
                })}
              >
                <Typography
                  sx={{ fontWeight: 'bold', fontSize: 18 }}
                  color={'inherit'}>
                  {link.title}
                </Typography>
              </NavLink>
            ))
          }
        </Box>
      </Toolbar>
    </AppBar>
  );
}

export default AppBarComponent;
