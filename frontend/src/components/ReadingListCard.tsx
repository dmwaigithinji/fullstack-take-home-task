import { Box, Card, CardContent, Grid, Paper, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

interface PropType {
  listName: string;
  coverPhotoURLs: string[];
}

const ReadingListCard: React.FC<PropType> = ({ listName, coverPhotoURLs }) => {

  return (
    <Link
      to={`/reading-list/${listName}`}
      style={{ textDecoration: 'none', color: 'inherit' }}>
      <Card sx={{  boxShadow: 'none' }}>
        <ReadingListImage coverPhotoUrls={coverPhotoURLs} />
        <CardContent>
          <Typography variant='h6' textAlign={'center'} sx={{ fontWeight: 'bold' }} >
            {listName}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
}

export default ReadingListCard;


const ReadingListImage: React.FC<{coverPhotoUrls: string[]}> = ({coverPhotoUrls}) => {
  if (!coverPhotoUrls?.length) {
    return <NoBooksInList />
  }

  if (coverPhotoUrls.length < 4) {
    return <SingleImage coverPhotoUrl={coverPhotoUrls[0]} />
  }

  return (
    <ImageGrid coverPhotoUrls={coverPhotoUrls} />
  )
}

const NoBooksInList: React.FC = () => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        paddingTop: '100%',
        position: 'relative',
        width: '100%',
        borderRadius: '20px',
        overflow: 'hidden',
        backgroundColor: '#5ACCCC',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        boxShadow: 0,
      }}
    >
      <Typography
        variant="h6"
        component="div"
        textAlign="center"
        color="white"
        sx={{ position: 'absolute', top: '25%', p: 2, fontWeight: 'bold' }}>
        No books in<br />this list yet
      </Typography>
    </Box>
  );
}

const SingleImage: React.FC<{coverPhotoUrl: string}> = ({coverPhotoUrl}) => {
  return (
    <Box
      sx={{
        paddingTop: '100%',
        position: 'relative',
        width: '100%',
        borderRadius: '20px',
        overflow: 'hidden',
        backgroundImage: `url(${coverPhotoUrl})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        boxShadow: 0,
      }}
    >
    </Box>
  )
}

const ImageGrid: React.FC<{ coverPhotoUrls: string[] }> = ({ coverPhotoUrls }) => {
  return (
    <Grid container style={{ overflow: 'hidden', borderRadius: 20 }}>
      {coverPhotoUrls.map((url, index) => (
        <Grid item xs={6} key={index} style={{ padding: 0 }}>
          <Paper
            sx={{
              paddingTop: '100%',
              width: '100%',
              backgroundImage: `url(${url})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              m: 0,
              borderRadius: 0,
              position: 'relative'
            }}
          >
            <div style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundImage: `url(${url})`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }} />
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
};

