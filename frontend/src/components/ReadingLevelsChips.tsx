import { Box, Chip, IconButton, Typography } from "@mui/material";
import { useRef } from "react";
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

const SCROLL_OFFSET = 200;

interface PropTypes {
  readingLevels: string[];
  selectedReadingLevel?: string;
  onClick: (level: string) => void;
}

const ReadingLevelsChips: React.FC<PropTypes> = ({
  readingLevels,
  selectedReadingLevel,
  onClick
}) => {

  const handleChipClick = (level: string) => {
    onClick(level === 'All' ? '' : level);
  };

  const scrollRef = useRef(null);

  const scroll = (scrollOffset: number) => {
    if (scrollRef.current) {
      (scrollRef.current as any).scrollBy({
        left: scrollOffset,
        behavior: 'smooth'
      });
    }
  };

  if (!readingLevels?.length ) {
    return null;
  }

  return (
    <Box>
      <Typography gutterBottom color={'textSecondary'} sx={{ pb: 1 }}>Filter by reading level</Typography>
      <Box sx={{ display: 'flex', alignItems: 'center', overflow: 'hidden' }}>
        <IconButton onClick={() => scroll(-SCROLL_OFFSET)} size="small" >
          <ArrowBackIosIcon fontSize="small" />
        </IconButton>
        <Box ref={scrollRef} sx={{ overflowX: 'auto', display: 'flex', flexGrow: 1, scrollbarWidth: 'none', msOverflowStyle: 'none', '&::-webkit-scrollbar': { display: 'none' } }}>
        <Chip
          label={'All'}
          clickable
          onClick={() => handleChipClick('')}
          color={selectedReadingLevel === '' ? 'secondary' : 'default'}
          variant={selectedReadingLevel === '' ? 'filled' : "outlined"}
          sx={{ marginX: 0.5 }}
        />
        {readingLevels.map((level: string, index: number) => (
            <Chip
              key={index}
              label={`Level ${level}`}
              onClick={() => handleChipClick(level)}
              clickable
              color={selectedReadingLevel === level ? 'secondary' : 'default'}
              variant={selectedReadingLevel === level ? 'filled' : "outlined"}
              sx={{ marginX: 0.5 }}
            />
          ))}
        </Box>
        <IconButton onClick={() => scroll(SCROLL_OFFSET)} size="small">
          <ArrowForwardIosIcon fontSize="small" />
        </IconButton>
      </Box>
    </Box>
  );
};

export default ReadingLevelsChips;
