import { memo } from 'react';
import { Grid } from '@mui/material';
import { DisplayBook } from '../types/Book.interface';
import BookCard from './BookCard';
import LoadingGrid from './LoadingGrid';

interface PropType {
  books: DisplayBook[];
  isLoading?: boolean;
}

const BookGrid: React.FC<PropType> = ({
  books,
  isLoading,
}) => {

  if (isLoading) {
    return (<LoadingGrid skeletonCount={4} height={150} />)
  }

  return (
    <Grid container spacing={3}>
      {books.map(
        (book: DisplayBook, index: number) => (
          <Grid
            key={index + book.title}
            item
            xs={6} sm={4} md={3} lg={2}
          >
            <BookCard
              id={book.id}
              title={book.title}
              author={book.author}
              coverPhotoURL={book.coverPhotoURL}
              readingLevel={book.readingLevel}
              actionText={book.actionText}
              action={book.action}
            />
          </Grid>
        ))
      }
  </Grid>
  );
}

export default memo(BookGrid);
