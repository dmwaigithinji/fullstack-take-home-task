/** A list of sentences */
type Page = string[]

export interface Book {
  id: string;
  title: string;
  author: string;
  coverPhotoURL: string;
  readingLevel: string;
  pages: Page[];
}

export interface DisplayBook extends Book {
  actionText?: string;
  action?: (bookTitle: string, author: string) => void;
}
