export const typeDefs = `#graphql
  type Book {
    title: String
    author: String
    coverPhotoURL: String
    readingLevel: String
    id: String
    pages: [[String]]
  }

  input  Search {
    bookTitle: String
  }

  input Filters {
    readingLevel: String
  }

  input PaginationArgs {
    page: Int!
    pageSize: Int!
  }

  type PageInfo {
    pageSize: Int!
    currentPage: Int!
    totalPages: Int!
  }

  type PaginatedBooks {
    books: [Book]
    pageInfo: PageInfo
  }


  type ReadingList {
    name: String
    noOfBooks: Int
    books(paginationArgs: PaginationArgs): PaginatedBooks
  }

  type Query {
    books(paginationArgs: PaginationArgs, search: Search, filters: Filters): PaginatedBooks
    readingLevels: [String]
    readingLists: [ReadingList]
    readingList(listName: String!, paginationArgs: PaginationArgs): ReadingList
    bookById(id: String!): Book
  }

  type AddBookToReadingListPayload {
    listName: String
    addedBook: Book
  }

  type RemoveBookFromReadingListPayload {
    listName: String
    removed: Boolean
  }

  type Mutation {
    addBookToReadingList(listName: String!, bookTitle: String!, author: String!): AddBookToReadingListPayload
    removeBookFromReadingList(listName: String!, bookTitle: String!, author: String!): RemoveBookFromReadingListPayload
  }
`;
