import { Book, EnrichedBook } from './types';

// Poems generated from Claude.ai based on reading level
const A = [
  {
    title: 'Happy Dragon and the Magic Spell',
    poem: 'The dragon\'s smile grew wide as he cast a spell of joy.'
  },
  {
    title: 'Lucky Monster and the Magic Spell',
    poem: 'With a wave of its paw, the monster\'s luck turned gold.'
  },
  {
    title: 'Happy Wizard and the Enchanted Garden',
    poem: 'The wizard\'s wand made flowers dance and sing with glee.'
  },
  {
    title: 'Lucky Robot and the Hidden Treasure',
    poem: 'Beep-boop, the robot found a chest of shiny gems!'
  },
  {
    title: 'Adventurous Monster and the Starry Sky',
    poem: 'The monster climbed high to touch twinkling stars.'
  },
  {
    title: 'Lucky Fairy and the Starry Sky',
    poem: 'Fairy dust sprinkled, making wishes come true under stars.'
  },
  {
    title: 'Adventurous Robot and the Moonlight Mystery',
    poem: 'Gears whirring, the robot solved the moon\'s glowing riddle.'
  },
  {
    title: 'Clever Princess on the Wonder Island',
    poem: 'The princess outsmarted puzzles on the magical isle.'
  },
  {
    title: 'Magic Dragon and the Enchanted Garden',
    poem: 'Fire-breath brought colorful blooms to life.'
  },
  {
    title: 'Happy Giant and the Magic Spell',
    poem: 'The giant\'s laughter boomed as he learned jolly tricks.'
  }
];

const B = [
  {
    title: 'Clever Wizard and the Starry Sky',
    poem: 'Stars twinkled at his command. The wizard\'s wit lit the land.'
  },
  {
    title: 'Magic Wizard and the Great Adventure',
    poem: 'Spells opened new worlds. The wizard bravely unfurled.'
  },
  {
    title: 'Little Fairy and the Enchanted Garden',
    poem: 'Tiny wings, rainbow blooms. Pixie dust made flowers zoom.'
  },
  {
    title: 'Happy Dragon and the Moonlight Mystery',
    poem: 'Scales glowed in moonlight. Dragon solved riddles bright.'
  },
  {
    title: 'Shiny Monster and the Moonlight Mystery',
    poem: 'Fur gleamed under stars. Monster found moon\'s secret jars.'
  },
  {
    title: 'Magic Dragon and the Secret Forest',
    poem: 'Fire lit ancient trees. Leaves sang in the breeze.'
  },
  {
    title: 'Lucky Knight and the Tower of Dreams',
    poem: 'Fortune helped him climb. Wishes chimed just in time.'
  },
  {
    title: 'Brave Fairy and the Hidden Treasure',
    poem: 'Wings led to gold chest. Courage shone the best.'
  },
  {
    title: 'Shiny Knight and the Great Adventure',
    poem: 'Armor gleamed on quests. Knight met every test.'
  },
  {
    title: 'Lucky Giant and the Moonlight Mystery',
    poem: 'Giant found glowing stone. Moon secrets were shown.'
  },
  {
    title: 'Brave Pirate and the Enchanted Garden',
    poem: 'Pirate met magic flowers. Nature matched his powers.'
  }
];
const C =[
  {
    title: 'Happy Knight and the Magic Spell',
    poem: 'The knight smiled brightly. His armor glowed with magic. Happiness spread through the kingdom.'
  },
  {
    title: 'Happy Pirate and the Tower of Dreams',
    poem: 'The jolly pirate climbed the tower. Dreams floated like clouds. His laughter echoed in the sky.'
  },
  {
    title: 'Brave Robot and the Starry Sky',
    poem: 'The robot gazed at twinkling stars. It beeped with courage. The sky welcomed its metal friend.'
  },
  {
    title: 'Magic Princess and the Hidden Treasure',
    poem: 'The princess waved her wand. A secret door appeared. Treasure gleamed in the magical light.'
  },
  {
    title: 'Magic Robot and the Hidden Treasure',
    poem: 'The robot\'s eyes lit up. Its sensors detected gold. Magic and technology found the treasure.'
  },
  {
    title: 'Happy Pirate and the Secret Forest',
    poem: 'The pirate sang a merry tune. Trees whispered ancient secrets. The forest joined his happy song.'
  },
  {
    title: 'Magic Monster and the Moonlight Mystery',
    poem: 'The monster cast a spell. Moonbeams danced around it. The night\'s mystery was revealed.'
  },
  {
    title: 'Clever Knight and the Secret Forest',
    poem: 'The knight solved tricky riddles. Trees bowed to his wisdom. The forest shared its secrets.'
  },
  {
    title: 'Adventurous Monster and the Moonlight Mystery',
    poem: 'The monster explored the night. Moonlight guided its path. A hidden world was discovered.'
  },
  {
    title: 'Magic Knight and the Hidden Treasure',
    poem: 'The knight\'s sword sparkled with magic. A map appeared in the air. Treasure awaited the brave hero.'
  },
  {
    title: 'Lucky Unicorn and the Enchanted Garden',
    poem: 'The unicorn\'s horn shimmered. Flowers bloomed in its path. Luck filled the magical garden.'
  },
  {
    title: 'Shiny Robot and the Hidden Treasure',
    poem: 'The robot\'s metal gleamed brightly. Its scanners found a hidden chest. Jewels shone like stars.'
  },
  {
    title: 'Happy Unicorn and the Great Adventure',
    poem: 'The unicorn pranced with joy. A rainbow path appeared. An exciting journey began.'
  }
];
const D = [
  {
    title: 'Magic Princess and the Enchanted Garden',
    poem: 'The princess waved her wand with grace. Flowers bloomed in every place. Trees whispered secrets old and new. In her garden, dreams came true.'
  },
  {
    title: 'Happy Princess and the Moonlight Mystery',
    poem: 'Her laughter rang through silver night. Moon beams danced with pure delight. A riddle in the shadows lay. The princess solved it with a play.'
  },
  {
    title: 'Big Princess and the Starry Sky',
    poem: 'The giant princess touched the stars. Her crown brushed Saturn, Jupiter, Mars. She giggled, making comets swirl. The sky a playground for this girl.'
  },
  {
    title: 'Magic Giant and the Hidden Treasure',
    poem: 'The giant\'s spell shook mountains deep. Gold and gems from earth did leap. His magic found what none could see. A treasure big as he could be.'
  },
  {
    title: 'Lucky Knight and the Lost World',
    poem: 'Fortune smiled on armor bright. The knight found paths both left and right. Each choice led to wonders new. In the lost world, his luck held true.'
  },
  {
    title: 'Lucky Monster and the Lost World',
    poem: 'Scales gleaming with fortune\'s light. The monster roamed both day and night. In the lost world, it found a home. Where lucky stars forever roam.'
  },
  {
    title: 'Happy Princess and the Starry Sky',
    poem: 'Her joy lit up the darkest space. Stars twinkled at her smiling face. She danced on clouds with cheerful grace. Happiness filled every place.'
  },
  {
    title: 'Brave Robot and the Secret Forest',
    poem: 'Gears whirring, the robot strode. Through ancient woods, a secret code. No fear of dark or tangled vine. Its courage made the forest shine.'
  },
  {
    title: 'Little Princess and the Enchanted Garden',
    poem: 'Though small, her heart was garden-wide. Magic blooms grew by her side. Tiny hands tended with care. Beauty sprouted everywhere.'
  },
  {
    title: 'Curious Monster and the Great Adventure',
    poem: 'Eyes wide, the monster roamed afar. Over mountains, under stars. Each new sight a wondrous find. Adventure filled its curious mind.'
  },
  {
    title: 'Curious Knight and the Tower of Dreams',
    poem: 'Questions led the knight up high. In the tower that touched the sky. Each floor revealed a new surprise. Curiosity opened dreaming eyes.'
  },
  {
    title: 'Shiny Giant and the Moonlight Mystery',
    poem: 'Polished skin gleamed in moon\'s glow. The giant\'s shadow stretched below. A puzzle in the light was born. Solved by dawn\'s early morn.'
  }
];
const E = [
  {
    title: 'Happy Giant and the Starry Sky',
    poem: 'Giant smiled at twinkling stars. He reached up with gentle hands. Stars danced on his fingertips. Giant laughed with pure delight. The sky sparkled with his joy.'
  },
  {
    title: 'Happy Monster and the Lost World',
    poem: 'Monster found a hidden door. Behind it, a world unknown. He explored with cheerful steps. New friends greeted him warmly. Monster\'s heart filled with wonder.'
  },
  {
    title: 'Curious Knight on the Wonder Island',
    poem: 'Knight landed on magic shores. He asked questions everywhere. Talking trees shared ancient tales. Mystic caves echoed secrets. Knight\'s mind grew with each answer.'
  },
  {
    title: 'Happy Princess and the Tower of Dreams',
    poem: 'Princess climbed the spiral stairs. Each floor held a new surprise. She giggled at silly sights. Dreams danced in colorful swirls. Princess found joy in each room.'
  },
  {
    title: 'Big Fairy and the Starry Sky',
    poem: 'Fairy grew as tall as trees. She touched clouds with her wings. Stars nestled in her long hair. Moon smiled at her kindly. Fairy hugged the whole night sky.'
  },
  {
    title: 'Happy Fairy and the Magic Spell',
    poem: 'Fairy waved her glowing wand. Sparks flew in rainbow hues. Flowers bloomed, birds sang sweetly. Sad faces turned to smiles. Her spell spread happiness everywhere.'
  },
  {
    title: 'Brave Giant and the Lost World',
    poem: 'Giant stepped through misty veil. Strange creatures greeted him. He faced challenges with courage. Helped new friends in need. Giant\'s bravery saved the day.'
  },
  {
    title: 'Lucky Robot and the Enchanted Garden',
    poem: 'Robot rolled into magic blooms. Flowers beeped friendly hellos. Butterfly kisses charged his battery. Fruit trees shared tasty oil. Robot found his lucky place.'
  },
  {
    title: 'Curious Monster and the Starry Sky',
    poem: 'Monster gazed at night sky. He wondered about each star. Rocket pack helped him explore. He met aliens and comets. Monster\'s curiosity led to friendship.'
  }
];
const F = [
  {
    title: 'Shiny Pirate and the Secret Forest',
    poem: 'A pirate gleamed in the sun. He found a map to a hidden wood. Trees whispered ancient secrets. Glowing mushrooms lit his path. He discovered a magical spring. The pirate\'s heart shone brighter still.'
  },
  {
    title: 'Brave Wizard and the Starry Sky',
    poem: 'The wizard raised his glowing staff. Stars twinkled in response. He cast a spell of twinkling light. Constellations danced for him. Galaxies swirled at his command. The wizard\'s courage lit the night.'
  },
  {
    title: 'Lucky Giant and the Hidden Treasure',
    poem: 'A giant stumbled on a clue. Fortune smiled as he searched. Rainbows led to a secret cave. Golden coins filled his pockets. Jewels sparkled in his hands. Luck made the giant rich indeed.'
  },
  {
    title: 'Brave Monster and the Secret Forest',
    poem: 'A monster ventured into woods unknown. Shadows loomed, but he pressed on. He befriended talking trees. Helped a fairy fix her wings. Faced a dragon with a smile. The monster\'s bravery won the day.'
  },
  {
    title: 'Brave Fairy and the Hidden Treasure',
    poem: 'A tiny fairy with a big heart. She flew through dangers untold. Crossed raging rivers on a leaf. Climbed mountains tall as clouds. Found a chest of magic dust. Her courage was the real treasure.'
  },
  {
    title: 'Adventurous Pirate and the Enchanted Garden',
    poem: 'The pirate sailed to a magic shore. Flowers sang sea shanties to him. Fruit trees offered tasty gold. Butterflies painted his ship anew. He swung on vines of silver. Adventure bloomed in every step.'
  },
  {
    title: 'Little Dragon and the Moonlight Mystery',
    poem: 'A small dragon saw the moon grow dark. He flew up to investigate. Moonbeams had tangled in star-webs. With gentle claws, he set them free. The moon smiled, bright once more. The little dragon solved the mystery.'
  },
  {
    title: 'Curious Wizard and the Moonlight Mystery',
    poem: 'The wizard wondered why stars dimmed. He brewed a potion of liquid light. Flew to the moon on his magic broom. Found moon-mice nibbling starlight. He shared his potion with them. Curiosity restored the night\'s glow.'
  },
  {
    title: 'Brave Fairy and the Magic Spell',
    poem: 'A fairy faced an evil curse. She gathered courage in her wings. Mixed dewdrops with sunbeam dust. Sang a song of ancient power. The curse broke with a flash. Her bravery created the strongest spell.'
  },
  {
    title: 'Shiny Knight on the Wonder Island',
    poem: 'Armor gleaming, the knight arrived. Palm trees bowed, shells sang hello. He polished dolphins to a sparkle. Made rainbows with his shiny shield. Taught mermaids to shine their scales. The island glowed with wonder.'
  },
  {
    title: 'Curious Fairy and the Enchanted Garden',
    poem: 'A fairy asked why flowers changed colors. She tickled petals to hear them giggle. Listened to roots tell underground tales. Watched bees dance their pollen stories. Learned how sunlight paints the blooms. Her curiosity made the garden grow.'
  },
  {
    title: 'Curious Unicorn and the Lost World',
    poem: 'A unicorn found a forgotten realm. She poked her horn in every corner. Asked the wind about its secrets. Nuzzled ancient sleeping stones. Woke up long-lost magic creatures. Her questions brought the world to life.'
  },
  {
    title: 'Magic Dragon on the Wonder Island',
    poem: 'A dragon landed, trailing sparkles. His scales reflected island colors. Breath of fire lit up caves. Wings stirred waves into rainbows. Roar awoke slumbering volcanoes. The dragon\'s magic merged with the isle.'
  }
];
const G = [
  {
    title: 'Happy Wizard and the Moonlight Mystery',
    poem: 'A jolly wizard gazed at the dim moon. His smile turned to a puzzled frown. Where had the moon\'s bright glow gone? He waved his wand with a cheerful flick. Sparks flew up, tickling the stars. The moon giggled, shaking off shadow dust. The wizard laughed, mystery solved with joy.'
  },
  {
    title: 'Lucky Pirate and the Enchanted Garden',
    poem: 'A pirate\'s ship drifted to a glowing shore. Flowers waved, inviting him to explore. Golden apples fell into his hands. Butterflies crowned him with petal hats. A wishing well granted him new friends. Rainbow fountains filled his treasure chest. The lucky pirate found fortune in bloom.'
  },
  {
    title: 'Big Robot and the Secret Forest',
    poem: 'A towering robot entered the whispering woods. Trees marveled at his shiny metal frame. He carefully stepped over tiny mushroom homes. Oil-can flowers offered him a refreshing drink. Squirrels used his shoulders as a playground. He learned to sing with the forest birds. The big robot found a home among the trees.'
  },
  {
    title: 'Curious Monster and the Lost World',
    poem: 'A monster peeked through a portal of mist. New colors and shapes caught his many eyes. He asked the swirling clouds their names. Tasted oddly shaped fruits with delight. Made friends with creatures of all kinds. Learned games played with starlight and shadows. His curiosity unveiled a world of wonders.'
  },
  {
    title: 'Shiny Dragon and the Lost World',
    poem: 'A dragon\'s scales glittered as he soared. He discovered a hidden realm below. His radiance lit up long-dark caverns. Ancient creatures awoke from their slumber. He polished dull gems with his bright breath. Forgotten cities sparkled in his glow. The shiny dragon brought light to the lost.'
  },
  {
    title: 'Shiny Monster and the Tower of Dreams',
    poem: 'A gleaming monster climbed the spiraling tower. Each floor reflected his dazzling skin. He found rooms filled with dancing dreams. His brightness made wish-stars shine brighter. Sleepy clouds used him as a night-light. He juggled moons to make dreamers laugh. The shiny monster became a beacon of joy.'
  }
];
const H = [
  {
    title: 'Curious Princess and the Enchanted Garden',
    poem: 'A princess with bright eyes entered a magical garden. She asked flowers why they had such pretty colors. She learned how trees whisper through their roots. The princess watched moonflowers open at night. Giggling grass taught her about growing tall. Fountains showed her how water travels. Her curiosity made the garden even more magical.'
  },
  {
    title: 'Magic Wizard and the Tower of Dreams',
    poem: 'A wizard climbed the twisting stairs of a dream tower. Each floor held a different world of sleeping wishes. He sprinkled stardust to make good dreams sparkle. His wand chased away scary nightmares. The wizard made soft cloud beds for dreamers. Sweet lullabies flowed from his magic flute. The wizard became the guardian of happy dreams.'
  },
  {
    title: 'Little Pirate and the Tower of Dreams',
    poem: 'A tiny pirate sailed to a tall lighthouse of dreams. He climbed moonbeam ropes to the dreamy floors. Chests full of happy thoughts clinked as he passed. The pirate fished for ideas in a sea of imagination. He found a map to the sweetest dream island. Starfish showed him how to shine in the dark. The little pirate\'s adventure brought joyful dreams.'
  },
  {
    title: 'Magic Monster and the Starry Sky',
    poem: 'A colorful monster looked up at the twinkling stars. He picked a star that laughed in his furry paw. The monster carefully juggled shiny planets. He untangled star patterns like glittery knots. Playful comets zoomed through his rainbow fur. His tail painted beautiful lights in the sky. The monster\'s magic made the stars shine brighter.'
  },
  {
    title: 'Adventurous Giant and the Moonlight Mystery',
    poem: 'A brave giant saw the moon\'s light growing dim. He stepped into the sky on cloudy stairs. The giant found the moon covered in star-dust. He polished the moon until it sparkled again. Surprised astronauts waved from their spaceship. The giant smiled and waved back at them. His adventure brought the moonlight back.'
  },
  {
    title: 'Little Monster and the Enchanted Garden',
    poem: 'A tiny monster explored a magical flower garden. Tall flowers gave the little one cool shade. He splashed in a dewdrop with water fairies. Friendly bees gave him rides between flowers. He planted seeds that grew into laughing daisies. Fireflies taught him how to glow in the dark. The little monster found a home in the garden.'
  },
  {
    title: 'Big Wizard and the Hidden Treasure',
    poem: 'A giant wizard searched for a magic treasure. His long beard swept away dust to find clues. The wizard\'s laugh scared shadows from caves. He read maps written in tree language. Rivers moved when he asked them nicely. He found a small chest in his big hands. The wizard learned friendship was the real treasure.'
  },
  {
    title: 'Magic Wizard and the Lost World',
    poem: 'A wizard\'s spell opened a door to a forgotten world. He stepped through mist into a land of wonders. His magic woke sleeping magical creatures. Forgotten languages came alive with his wand. The wizard\'s potions made stone forests grow. He wrote down the lost world\'s stories. His magic brought the forgotten realm back to life.'
  },
  {
    title: 'Brave Fairy and the Enchanted Garden',
    poem: 'A small fairy flew into a mysterious garden. She faced scary thorns without fear. Her magic dust helped shy flowers bloom. The fairy cheered up a grumpy old tree. She rescued tiny seeds from a deep pond. She taught mean plants to be kind. The brave fairy\'s spirit made the garden happy.'
  },
  {
    title: 'Clever Robot and the Lost World',
    poem: 'A smart robot found an ancient hidden world. It solved the puzzle of strange old symbols. The robot connected the past to the present. It learned wisdom from long-ago times. The robot woke up sleeping old machines. It answered questions unsolved for ages. Its clever mind brought new life to the lost world.'
  }
];
const I = [
  {
    title: 'Clever Monster on the Wonder Island',
    poem: 'A smart monster washed up on a magical shore. Puzzle trees asked riddles he eagerly solved. He built a raft from giggling driftwood. The monster befriended a wise, old turtle. He learned to speak with chattering shells. Rainbow fish taught him to swim in color. He created a map of the island\'s secrets. The clever monster made Wonder Island even more wonderful.'
  },
  {
    title: 'Happy Knight and the Magic Spell',
    poem: 'A jolly knight found a book of spells. His laughter made the words glow bright. He cast a spell that made flowers sing. His armor sparkled with newfound magic. The knight turned rain into candy drops. He taught his horse to fly with happy thoughts. Villagers smiled wherever he wandered. The happy knight\'s spell brought joy to all.'
  },
  {
    title: 'Shiny Giant and the Secret Forest',
    poem: 'A gleaming giant stepped into a hidden wood. His bright skin lit up shadowy paths. Squirrels used him as a shiny slide. He polished dull leaves until they sparkled. The giant\'s glow woke sleeping fairies. He decorated trees with his twinkling tears. His radiance scared away gloomy shadows. The shiny giant turned the secret forest magical.'
  },
  {
    title: 'Happy Fairy and the Lost World',
    poem: 'A cheerful fairy discovered a forgotten realm. Her laughter echoed through empty valleys. She sprinkled joy-dust on wilted plants. Grumpy rocks smiled at her silly jokes. The fairy painted rainbows on gray skies. She taught long-lost birds to sing again. Her happiness woke a sleeping sun. The happy fairy brought life back to the lost world.'
  },
  {
    title: 'Happy Knight and the Lost World',
    poem: 'A merry knight rode into an ancient land. His bright smile scared away creeping vines. He played tag with shy, forgotten creatures. The knight\'s jokes made old statues laugh. He planted seeds of cheer in barren fields. His kindness melted an enchanted ice wall. The knight\'s song woke a sleeping dragon. His happiness restored the lost world\'s magic.'
  },
  {
    title: 'Adventurous Robot and the Magic Spell',
    poem: 'A daring robot discovered a dusty spell book. Its circuits buzzed as it read magic words. The robot\'s eyes shot colorful beams. It turned rusty gears into golden butterflies. The robot learned to float on magic bubbles. It befriended a grumpy, old wizard. Together they mixed potions of wonder. The adventurous robot became a magical machine.'
  },
  {
    title: 'Lucky Dragon on the Wonder Island',
    poem: 'A fortunate dragon landed on a magical isle. Four-leaf clovers grew wherever it stepped. The dragon found gems in every coconut. It won games with mischievous island sprites. Rainbows appeared when the dragon sneezed. It discovered a cave of endless wishes. The dragon\'s scales changed colors for good luck. Wonder Island became even luckier with the dragon.'
  }
];
const J = [
  {
    title: 'Magic Dragon and the Tower of Dreams',
    poem: 'A shimmering dragon soared to a tall, dreamy tower. Its scales changed colors with each floor it climbed. In one room, it found clouds soft as pillows. Another held stars that sang lullabies. The dragon breathed rainbow fire to light up dark dreams. It chased away nightmares with a flick of its tail. Kind dreams danced on the dragon\'s wide wings. At the top, it met the sandman and his magic dust. Together, they sent sweet dreams to sleeping children below.'
  },
  {
    title: 'Happy Giant and the Enchanted Garden',
    poem: 'A jolly giant skipped into a magical garden. Flowers tickled his toes, making him laugh. His happy tears watered thirsty plants. Butterflies braided flowers into his hair. The giant\'s smile made sunflowers grow taller. He gently hung wind chimes on tree branches. His whistle taught birds new, merry tunes. The giant\'s laughter scared away gloomy clouds. His joy spread, making the enchanted garden bloom brighter.'
  },
  {
    title: 'Adventurous Princess and the Enchanted Garden',
    poem: 'A brave princess ventured into a mysterious garden. She crossed a bridge made of twisting vines. Talking flowers whispered secrets in her ears. The princess solved riddles posed by wise old trees. She swung across a pond on a friendly vine. Magical fireflies lit her path through a hedge maze. The princess befriended a shy, invisible gardener. She planted seeds that grew into wishing wells. Her adventure transformed the garden into a wonderland.'
  },
  {
    title: 'Big Wizard and the Tower of Dreams',
    poem: 'A tall wizard entered a spiraling tower of dreams. His hat brushed the ceiling, sprinkling sleepy dust. He waved his wand, turning nightmares into butterflies. The wizard brewed potions of peaceful slumber. His gentle snores became soft lullabies. He painted dreamscapes with his magical beard. The wizard\'s kind words soothed restless thoughts. He knitted blankets from clouds and starlight. The big wizard became guardian of the sweetest dreams.'
  },
  {
    title: 'Adventurous Pirate and the Hidden Treasure',
    poem: 'A daring pirate followed a mysterious map. She swung through jungles on vines of gold. Talking parrots gave her tricky clues. The pirate sailed across a sea of singing sand. She solved the riddle of the whispering caves. X marked the spot on a glowing beach. She dug with her lucky shovel under moonlight. Treasure sparkled: friends she made on her journey. The adventurous pirate\'s greatest find was her bravery.'
  },
  {
    title: 'Curious Wizard on the Wonder Island',
    poem: 'An inquisitive wizard landed on a magical isle. He asked the chatty palm trees about their secrets. Puzzled hermit crabs taught him shell magic. The wizard learned to surf on waves of rainbow. He studied the dance of the butterfly fish. Mystic sand dollars revealed ancient spells. The wizard brewed potions in coconut shells. He unraveled the mystery of the island\'s heart. His curiosity made Wonder Island even more wondrous.'
  },
  {
    title: 'Happy Dragon and the Lost World',
    poem: 'A cheerful dragon found a forgotten realm. Its joyful roar woke sleeping stone giants. The dragon\'s smile melted years of ice. Its laughter made long-dry rivers flow again. The dragon played tag with shy, ancient creatures. Its happy tears brought life to barren fields. The dragon\'s kind heart befriended lonely mountains. Its silly dances taught old trees to sway. The happy dragon restored joy to the lost world.'
  },
  {
    title: 'Big Robot and the Moonlight Mystery',
    poem: 'A giant robot noticed the moon growing dim. It built a ladder of stars to reach the sky. The robot found moon dust clogging lunar gears. It carefully cleaned each cog and spring. The robot oiled the moon with meteor shower drops. It adjusted the moon\'s tilt for better light. The robot taught moon rocks to glow again. It fixed the moon\'s broken smile servo. The big robot solved the mystery and restored moonlight.'
  },
  {
    title: 'Lucky Fairy and the Tower of Dreams',
    poem: 'A fortunate fairy fluttered into a dreamy tower. Four-leaf clovers sprouted where she landed. She sprinkled lucky dust on sleepy pillows. The fairy found a room of wishing wells. Her wand turned bad dreams into golden butterflies. She collected sweet dreams in her lucky hat. The fairy\'s giggles chased away nightmare clouds. She wove good luck charms from moonbeams. The lucky fairy blessed the tower with happiest dreams.'
  }
];

const poems: {[key: string]: {title: string; poem: string}[]} = {A, B, C, D, E, F, G, H, I, J};

function splitPoemIntoPages(poem: string): string[][] {
  const sentences = poem.split('. ').filter(sentence => sentence.trim().length > 0);

  if (sentences.length <= 5) {
    return [sentences];  // All sentences fit on one page
  }
  const halfwayIndex = Math.ceil(sentences.length / 2);
  return [
    sentences.slice(0, halfwayIndex),
    sentences.slice(halfwayIndex)
  ];
}

function generateRandomId(length = 10) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function createEnrichedBooks(books: Book[]): EnrichedBook[] {
  return books.map(b => {
    const id = generateRandomId();
    const levelPoems = poems[b.readingLevel];
    const poem = levelPoems.find(p => p.title === b.title)?.poem || '';
    const pages = splitPoemIntoPages(poem);
    return { ...b, id, pages };
  });
}
