export interface Book {
  title: string;
  author: string;
  coverPhotoURL: string;
  readingLevel: string;
}

export interface PaginationArgs {
  page: number;
  pageSize: number;
}

/** a page is a list of sentences */
type Page = string[];

export interface EnrichedBook extends Book {
  id: string;
  pages: Page[];
}