import { enrichedBooks } from './enrichedBooks';
import { Book } from './types';

export interface ReadingList {
  name: string;
  books: Book[];
}

export const readingLists: ReadingList[] = [
  {
    name: 'Grade 1',
    books: enrichedBooks.filter(b => b.readingLevel === 'A').slice(0, 3),
  },
  {
    name: 'Grade 2',
    books: []
  },
  {
    name: 'Grade 3',
    books: enrichedBooks.filter(b => ['H', 'J'].includes(b.readingLevel)),
  }
];