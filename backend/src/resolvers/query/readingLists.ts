import { readingLists } from '../../data';
import { PaginationArgs } from '../../data/types';

export const readingListsQuery = () => {
  return readingLists.map(list => {
    // returning only four books max, since these are what are required to show readingList banner
    const books = list.books.slice(0, 4);
    return {
      ...list,
      noOfBooks: list.books.length,
      books: {
        books
      }
    };
  });
};

export const readingListQuery = (_: null, { listName, paginationArgs }: { listName: string, paginationArgs: PaginationArgs }) => {
  const readingList = readingLists.find((list) => list.name === listName);

  if (!readingList) {
    throw new Error(`Reading list with name "${listName}" not found.`);
  }

  const totalBooks = readingList.books.length;
  const sortedBooks = readingList.books.sort((a, b) => a.title.localeCompare(b.title));

  if (!paginationArgs) {
    return {
      name: readingList.name,
      noOfBooks: totalBooks,
      books: {
        books: sortedBooks,
      }
    };
  }

  const totalPages = Math.ceil(totalBooks / paginationArgs.pageSize);
  const startIndex = (paginationArgs.page - 1) * paginationArgs.pageSize;
  const paginatedBooks = sortedBooks.slice(startIndex, startIndex + paginationArgs.pageSize);

  return {
    name: readingList.name,
    noOfBooks: totalBooks,
    books: {
      books: paginatedBooks,
      pageInfo: {
        pageSize: paginationArgs.pageSize,
        currentPage: paginationArgs.page,
        totalPages: totalPages,
      },
    }
  };
};