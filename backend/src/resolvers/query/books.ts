import { enrichedBooks, PaginationArgs } from '../../data';

interface Search {
  bookTitle?: string;
}

interface Filters {
  readingLevel?: string;
}

export const booksQuery = (_: null, { paginationArgs, search, filters }: { paginationArgs: PaginationArgs, search?: Search, filters?: Filters }) => {
  let filteredBooks = enrichedBooks.sort((a, b) => a.title.localeCompare(b.title));

  if (search?.bookTitle) {
    const searchTerm = search.bookTitle.toLowerCase();
    filteredBooks = filteredBooks.filter(book => book.title.toLowerCase().includes(searchTerm));
  }

  if (filters?.readingLevel) {
    filteredBooks = filteredBooks.filter(book => book.readingLevel === filters.readingLevel);
  }

  if (!paginationArgs) {
    return {
      books: filteredBooks,
    };
  }

  const totalBooks = filteredBooks.length;
  const totalPages = Math.ceil(totalBooks / paginationArgs.pageSize);
  const startIndex = (paginationArgs.page - 1) * paginationArgs.pageSize;
  const paginatedBooks = filteredBooks.slice(startIndex, startIndex + paginationArgs.pageSize);

  return {
    books: paginatedBooks,
    pageInfo: {
      pageSize: paginationArgs.pageSize,
      currentPage: paginationArgs.page,
      totalPages: totalPages,
    },
  };
};

export const readingLevelsQuery = () => {
  const readingLevels = Array.from(new Set(enrichedBooks.map(b => b.readingLevel)));
  return readingLevels.sort((a, b) => a.localeCompare(b));
};

export const bookByIdQuery = (_: null, { id }: { id: string }) => {
  const book = enrichedBooks.find(book => book.id === id);
  if (book) {
    return book;
  }
  throw new Error(`Book with id ${id} not found`);
};
