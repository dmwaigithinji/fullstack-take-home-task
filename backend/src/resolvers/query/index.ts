import { bookByIdQuery, booksQuery, readingLevelsQuery } from './books';
import { readingListsQuery, readingListQuery } from './readingLists';

export const Query = {
  books: booksQuery,
  readingLevels: readingLevelsQuery,
  readingLists: readingListsQuery,
  readingList: readingListQuery,
  bookById: bookByIdQuery,
};