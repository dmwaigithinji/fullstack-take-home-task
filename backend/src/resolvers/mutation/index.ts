import { addBookToReadingList, removeBookFromReadingList } from './readingLists';

export const Mutation = {
  addBookToReadingList,
  removeBookFromReadingList,
};
