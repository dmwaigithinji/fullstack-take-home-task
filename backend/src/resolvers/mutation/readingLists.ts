import { enrichedBooks, readingLists } from '../../data';

interface ModifyReadingListBooksArgs {
  listName: string;
  bookTitle: string;
  author: string;
}

export const addBookToReadingList = (_: null, { listName, bookTitle, author }: ModifyReadingListBooksArgs) => {
  const readingList = readingLists.find((list) => list.name === listName);
  const book = enrichedBooks.find((b) => b.title === bookTitle && b.author === author);
  if (readingList && book) {
    readingList.books.push(book);
    return {
      listName,
      addedBook: book
    };
  }
  throw new Error(`Reading list with name "${listName}" not found.`);
};

export const removeBookFromReadingList = (_: null, { listName, bookTitle, author }: ModifyReadingListBooksArgs) => {
  const readingList = readingLists.find((list) => list.name === listName);
  if (readingList) {
    readingList.books = readingList.books.filter((b) => !(b.title === bookTitle && b.author === author));
    return {
      listName,
      removed: true
    };
  }
  throw new Error(`Reading list with name "${listName}" not found.`);
};
