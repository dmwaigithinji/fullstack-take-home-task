# Fullstack Takehome Challenge - Books Listing App

This is a simple book listing application that allows teachers to assign books to students.

A fronted is hosted on Vercel and can be accessed from [here](https://hello-teachers.vercel.app/reading-lists).

The backend is hosted on GoogleCloud Run and be accessed from [here](https://backend-h7rvxf5h4a-uc.a.run.app/).

![Book Reading List App](screenshot.png)

## Features implemented

- List books and book collections/reading lists
- Search for books by title
- Add books to reading lists
- Remove books from reading list
- Paginated search results of books
- Filter books by reading levels
- Generated poems based on the book titles and reading level using AI
- Enabled previewing of a book's content

## Main Technologies Used

- React
- GraphQL
- Apollo Client
- Material-UI
- Zustand

## Running the application

### To run the frontend application

Run the following commands from the root of the project

```bash
cd frontend # navigate to the frontend directory
npm install # install dependencies
npm start   # start application
```

### To run the backend application

You can run the backend application with npm/yarn. While in the backend directory, run the following commands

```bash
cd backend  # navigate to the backend directory
npm install # install dependencies
npm start   # start application
```

Your Node.js application should now be running and accessible at [http://localhost:4000](http://localhost:4000).

#### Alternatively

Build and run the app as a docker container using Docker Compose

Ensure you have docker installed and running. You can test this by running:

```bash
docker --version
```

Build the Docker image and start the container.

```bash
docker-compose up --build
```

If you make changes to the Dockerfile or the dependencies, you can rebuild the containers using:

```bash
docker-compose up --build
```

To stop the running containers, press Ctrl+C in the terminal where docker-compose up is running. Alternatively, you can use:

```bash
docker-compose down
```

## Design Drafts

Quick initial drafts of the design were done on [Figma](https://www.figma.com/proto/K4RtIl3ZGklg1xFjKsEYVc/Ello-Task---Book-Listing-App?node-id=5-786&t=st2p2ZxwqLh4gSaD-1&scaling=scale-down&content-scaling=fixed&page-id=0%3A1)

